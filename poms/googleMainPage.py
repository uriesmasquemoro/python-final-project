# selenium modules
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

"""
This class is the Google main page where you can perform a search
"""


class GoogleMainPage():
    # contructor method, where the driver, search and input xpath's are located
    def __init__(self, driver):
        self.driver = driver
        self.search = None
        self.xpath_search_input = "//input[@name='q']"

    """this method asks the user what does he want to search"""

    def ask_for_search(self):
        self.search = input('Type what you want to search: ')
        return True

    """this method writes on the input what the user typed before"""

    def write_search(self):
        wait = WebDriverWait(self.driver, 30)
        search_input = wait.until(expected_conditions.element_to_be_clickable(
            (By.XPATH, self.xpath_search_input)))
        search_input.send_keys(self.search)
        return True

    """this method writes on the input what the user typed before"""

    def press_enter(self):
        wait = WebDriverWait(self.driver, 30)
        search_input = wait.until(expected_conditions.element_to_be_clickable(
            (By.XPATH, self.xpath_search_input)))
        search_input.send_keys(Keys.ENTER)
        return True
