# selenium modules
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

from miscellaneous import colours as cl

"""
This class is the Bandcamp main page where you can perform a search
"""


class BandcampResultsPage():
    # contructor method, where the driver, search and input xpath's are located
    def __init__(self, driver):
        self.driver = driver
        self.search = None
        self.xpath_results = "//div[@class='itemtype']"
        self.xpath_results_title = "//div[@class='heading']//a"
        self.title_list = None  # storages titles
        self.results_list = None  # storages all results
        self.track_list = []  # storages just tracks
        self.titles_list = []  # storages title tracks
        self.number_of_results = 0

    def count_track_results(self):
        wait = WebDriverWait(self.driver, 60)

        self.results_list = wait.until(
            expected_conditions.presence_of_all_elements_located((By.XPATH, self.xpath_results)))

        self.title_list = wait.until(
            expected_conditions.presence_of_all_elements_located((By.XPATH, self.xpath_results_title)))

        for i, track in enumerate(self.results_list):
            if track.get_attribute('innerText') == 'TRACK':
                self.track_list.append(self.title_list[i].get_attribute('innerText'))
                self.titles_list.append(
                    self.title_list[i])

        return True

    def validate_number_of_tracks(self):
        if len(self.track_list) > 0:
            print(f'{cl.Colours.OKGREEN}\n:: RESULTS FOUND!')
            return True
        else:
            return False

    def click_on_track(self):
        self.titles_list[0].click()
        #print(self.driver.current_url)
        print(f'{cl.Colours.OKGREEN}\n:: OPENING TRACK PAGE')
        return True