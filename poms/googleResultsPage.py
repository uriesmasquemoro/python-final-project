# built-in libs
import unittest

# selenium modules
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

# miscellaneous modules
from miscellaneous import colours as cl


class GoogleResultsPage():
    # contructor method, where the driver, search and input xpath's are located
    def __init__(self, driver):
        self.driver = driver
        self.search = None
        self.xpath_search_input = "//div[@class='a4bIc']//input[@name='q']"
        self.xpath_results = "//h3[@class='LC20lb DKV0Md']"
        self.xpath_descriptions = "//span[@class='st']"
        self.results_list = None
        self.filtered_results = []
        self.descriptions_list = None

    """this method takes the value of the search input to be able
    to compare it with the found results"""

    def set_search(self):
        wait = WebDriverWait(self.driver, 30)

        searchInput = wait.until(expected_conditions.element_to_be_clickable(
            (By.XPATH, self.xpath_search_input)))

        self.search = searchInput.get_attribute('value').upper()

        return True

    """this method gets the results of the results page and
    storages them in a list"""

    def get_results(self):
        wait = WebDriverWait(self.driver, 60)

        self.results_list = wait.until(
            expected_conditions.presence_of_all_elements_located((By.XPATH, self.xpath_results)))

        return True

    """this method validates that there's a description
    for every result"""

    def validate_descriptions(self):
        wait = WebDriverWait(self.driver, 60)
        self.descriptions_list = wait.until(
            expected_conditions.presence_of_all_elements_located((By.XPATH, self.xpath_descriptions)))

        resultsLen = len(self.descriptions_list)
        descriptionsLen = len(self.descriptions_list)

        if resultsLen == descriptionsLen:
            print(f'\n{cl.Colours.OKGREEN}:: EVERY RESULT HAS A DESCRIPTION')
            return True

        return False

    """this method verifies if the results contain the
    searched word(s)"""

    def compare_results(self):
        for res in self.results_list:
            if self.search in res.get_attribute('innerText').upper():
                self.filtered_results.append(
                    res.get_attribute('innerText').upper())

        return True

    """this method prints the filtered results"""

    def print_filtered_results(self):
        print(
            f'\n{cl.Colours.WHITE}:: FOUND [{len(self.filtered_results)}] RESULTS WITH COINCIDENCE\n')

        for i, res in enumerate(self.filtered_results):
            print(f'{cl.Colours.WHITE}{i + 1}. {res}')

        return True
