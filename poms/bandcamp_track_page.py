# selenium modules
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

from miscellaneous import colours as cl

"""
This class is the Bandcamp main page where you can perform a search
"""


class BandcampTrackPage():
    # contructor method, where the driver, search and input xpath's are located
    def __init__(self, driver):
        self.driver = driver
        self.xpath_play_button = "//div[@class='playbutton']"
        self.xpath_play_button_active = "//div[@class='playbutton playing']"
        self.xpath_track_title = "//h2[@class='trackTitle']"
        self.xpath_artist = "//span[@itemprop='byArtist']//a"

    def click_on_play_button(self):
        wait = WebDriverWait(self.driver, 30)

        play_button = wait.until(expected_conditions.element_to_be_clickable(
            (By.XPATH, self.xpath_play_button)))
        play_button.click()

        return True

    def is_music_playing(self):
        wait = WebDriverWait(self.driver, 30)

        play_button = wait.until(expected_conditions.element_to_be_clickable(
            (By.XPATH, self.xpath_play_button_active)))

        return True

    def show_music_playing(self):
        wait = WebDriverWait(self.driver, 30)

        song = wait.until(expected_conditions.presence_of_element_located(
            (By.XPATH, self.xpath_track_title)))

        artist = wait.until(expected_conditions.presence_of_element_located(
            (By.XPATH, self.xpath_artist)))

        print(f'\n:: PLAYING {song} by {artist}')
        
        return True
        
    def ask_if_wants_to_play(self):
        wait = WebDriverWait(self.driver, 30)

        song = wait.until(expected_conditions.presence_of_element_located(
            (By.XPATH, self.xpath_track_title)))

        response = input(f'Do you want to play {song.get_attribute("innerText")}? (Y/n): ').lower()

        if response == 'y':
            self.click_on_play_button()
            self.is_music_playing()
            print(f'{cl.Colours.OKGREEN}\n:: MUSIC IS PLAYING B)')
            return True
        elif response == 'n':
            print(f'{cl.Colours.WHITE}\n:: YOU DO NOT WANT TO PLAY THE SONG')
            return True
        else:
            return False
