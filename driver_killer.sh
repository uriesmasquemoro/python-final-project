#!/bin/bash
# Use in case try... except didn't work
# it does not make sense to use it if the
# execution successfully ran, this code will
# kill every web driver running behind.
killall chromedriver80
killall chromedriver81
