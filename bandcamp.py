# built-in libraries
import sys
import unittest

# selenium modules
from selenium import webdriver

# poms & miscellaneous modules
from miscellaneous import setup as st
from miscellaneous import colours as cl
from poms import bandcamp_main_page as bmp
from poms import bandcamp_results_page as brp
from poms import bandcamp_track_page as btp


class TestBandCamp(unittest.TestCase):
    def test_bandcamp(self):
        # setup & driver config
        setup = st.Setup()
        driver = setup.get_driver()
        driver.get('https://bandcamp.com/')

        bandcamp_main_page = bmp.BandcampMainPage(driver)
        bandcamp_results_page = brp.BandcampResultsPage(driver)
        bandcamp_track_page = btp.BandcampTrackPage(driver)

        try:
            print(f'{cl.Colours.HEADER}:: BANDCAMP FROM TERMINAL')

            print('\n:: OPTIONS')
            print('[1] Search with Bandcamp')
            print('[2] Exit')

            option = input('\nType a valid option: ')

            if option == '1':
                self.assertTrue(
                    bandcamp_main_page.ask_for_search(), 'I could not ask :(')

                self.assertTrue(bandcamp_main_page.write_search(),
                                'I could not write what you want')

                self.assertTrue(bandcamp_main_page.press_enter(),
                                'I could not press enter')

                self.assertTrue(bandcamp_results_page.count_track_results(),
                                'I could not count the track results')

                self.assertTrue(bandcamp_results_page.validate_number_of_tracks(),
                                'I could not validate the number of tracks')

                self.assertTrue(bandcamp_results_page.click_on_track(),
                                'I could no click on track title')

                self.assertTrue(bandcamp_track_page.ask_if_wants_to_play(),
                                'I could not ask if user wants to play')
            elif option == '2':
                return
            else:
                print(f'{cl.Colours.WARNING}\n:: YOU DID NOT SELECT A VALID OPTION!')

            print(f'{cl.Colours.OKBLUE}\n:: GXIS LA REVIDO!\n')
            print(f'{cl.Colours.OKGREEN}:: MEMORY FREED')
            print(f'{cl.Colours.OKGREEN}:: EXIT SUCCESS')
            driver.quit()
        except Exception as e:
            print(f'\n{cl.Colours.FAIL}:: {e}')
            print(f'{cl.Colours.OKGREEN}:: MEMORY FREED')
            driver.quit()


if __name__ == '__main__':
    unittest.main()
