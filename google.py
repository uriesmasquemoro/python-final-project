# built-in libraries
import sys
import unittest

# selenium modules
from selenium import webdriver

# poms & miscellaneous modules
from miscellaneous import setup as st
from miscellaneous import colours as cl
from poms import googleMainPage as gmp
from poms import googleResultsPage as grp


class TestGoogle(unittest.TestCase):
    def test_google(self):
        # setup & driver config
        setup = st.Setup()
        driver = setup.get_driver()
        driver.get('https://google.com/')

        # google main page instance
        google_main_page = gmp.GoogleMainPage(driver)
        google_results_page = grp.GoogleResultsPage(driver)

        try:
            print(f'{cl.Colours.HEADER}:: GOOGLE SEACH ENGINE FROM TERMINAL')
            self.assertTrue(google_main_page.ask_for_search(),
                            'I could not ask for search')

            self.assertTrue(google_main_page.write_search(),
                            'I could not write what you wanted to search')

            self.assertTrue(google_main_page.press_enter(),
                            'I could not press enter')

            self.assertTrue(google_results_page.set_search(),
                            'I could not set the search')

            self.assertTrue(google_results_page.get_results(),
                            'I could not get the results')

            self.assertTrue(google_results_page.validate_descriptions(),
                            'I could not validate descriptions')

            self.assertTrue(google_results_page.compare_results(),
                            'I could not compare the results')

            self.assertTrue(google_results_page.print_filtered_results(),
                            'I could not print the filtered results')

            print(f'{cl.Colours.OKBLUE}\n:: GXIS LA REVIDO!\n')
            print(f'{cl.Colours.OKGREEN}:: MEMORY FREED')
            print(f'{cl.Colours.OKGREEN}:: EXIT SUCCESS')
            driver.quit()
        except:
            print(f'{cl.Colours.FAIL}:: {sys.exc_info()[0]}')
            print(f'{cl.Colours.OKGREEN}:: MEMORY FREED')
            driver.quit()


if __name__ == '__main__':
    unittest.main()
